package es.dxc.flic.MainActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import es.dxc.flic.R;
import es.dxc.flic.bean.Photo;


public class PhotoAdapter extends  RecyclerView.Adapter<PhotoAdapter.MyViewHolder> {

    ArrayList <Photo> fotos;


    public PhotoAdapter(ArrayList <Photo> fotos) {
        this.fotos = fotos;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_fotos, parent, false);

        final MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;

    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Picasso.get()
                .load("https://live.staticflickr.com/"+ fotos.get(position).getServer()+"/"+fotos.get(position).getId()+"_"+fotos.get(position).getSecret()+".jpg")
                .placeholder(R.drawable.imagen)
                .error(R.drawable.imagen)
                .into(holder.logo);



        holder.titulo.setText(fotos.get(position).getTitle().get_Content());
        holder.autor.setText(fotos.get(position).getOwner().getUsername());
        holder.masInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Photo foto = fotos.get(position);

                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                PhotoDetalleFragment fragment = PhotoDetalleFragment.newInstance(foto);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.MainActivity, fragment,"inicio");
                transaction.addToBackStack("inicio");
                transaction.commit();




            }
        });

    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        public LinearLayout masInfo;
        public ImageView logo;
        public TextView titulo,autor;

        public MyViewHolder(View itemView) {
            super(itemView);
            logo = (ImageView)itemView.findViewById(R.id.ivImagen);
            titulo = (TextView)itemView.findViewById(R.id.textotitulo);
            autor = (TextView)itemView.findViewById(R.id.textoautor);
            masInfo = (LinearLayout) itemView.findViewById(R.id.fotoinfo);

        }
    }

    @Override
    public int getItemCount() {
        return fotos.size();
    }

}
  
