package es.dxc.flic.MainActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hrskrs.instadotlib.InstaDotView;

import java.util.ArrayList;
import java.util.Iterator;

import es.dxc.flic.MainActivity.Task.PhotoIdDetailListadoTask;
import es.dxc.flic.MainActivity.Task.PhotoIdListadoTask;
import es.dxc.flic.R;
import es.dxc.flic.bean.DetailResponse;
import es.dxc.flic.bean.Photo;
import es.dxc.flic.bean.Photoid;
import es.dxc.flic.bean.PhotoidList;
import es.dxc.flic.bean.SearchResponse;
import es.dxc.flic.utils.OnSwipeTouchListener;


public class MainActivity extends AppCompatActivity implements PhotoIdListadoTask.AsyncResponse, PhotoIdDetailListadoTask.AsyncResponse {

    ImageView BotonBuscar;
    EditText Textobusqueda;

    private PhotoIdListadoTask listTask = null;
    private PhotoIdDetailListadoTask listdetailTask = null;

    private ProgressBar progressBar;

    ArrayList arrayListphotoid;
    ArrayList listphotodetail;
    private ArrayList <Photo> fotos;
    ArrayList <Photo> fotosDel;
    RecyclerView rvMain;
    private static final long FOTOS = 5;
    int numpagh = 0;
    double numPag;
    InstaDotView instaDotView;
    private int page = 0;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progress);


        rvMain = (RecyclerView) findViewById(R.id.rvMainFotos);
        instaDotView = findViewById(R.id.instadot);

        BotonBuscar = (ImageButton) findViewById(R.id.imageButtonBuscador);
        Textobusqueda = (EditText) findViewById(R.id.palabra_clave);

        BotonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textobusqueda = Textobusqueda.getText().toString();
                if (checktexto(textobusqueda)) {
                    getPhoid(textobusqueda, getBaseContext());
                    showProgress();
                } else {

                    Toast toast = Toast.makeText(getBaseContext(), R.string.errortexto, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }
            }
        });
    }

    private Boolean checktexto(String textobusqueda) {

        if (textobusqueda != null && !textobusqueda.equalsIgnoreCase("")) {
            return true;
        } else {
            return false;
        }
    }

    private void getPhoid(String textobusqueda, Context context) {
        listTask = new PhotoIdListadoTask(textobusqueda, context);
        listTask.response = this;
        listTask.execute((Void) null);
    }

    private void getPhoidDetail(String id, Context context) {
        listdetailTask = new PhotoIdDetailListadoTask(id, context);
        listdetailTask.response = this;
        listdetailTask.execute((Void) null);
    }

    @Override
    public void processFinish(SearchResponse photoids) {

        if (photoids != null&&photoids.getPhotos().getPhotoIdList().size()>0) {

            arrayListphotoid = new ArrayList();
            listphotodetail = new ArrayList <Photo>();

            SearchResponse searchResponse = photoids;
            PhotoidList arrayphoto = searchResponse.getPhotos();
            Iterator <Photoid> iterator = arrayphoto.getPhotoIdList().iterator();
            while (iterator.hasNext()) {
                Photoid photo = iterator.next();
                arrayListphotoid.add(photo.getPhotoId());
            }

            getListPhoidDetail(arrayListphotoid);
        } else if(photoids.getPhotos().getPhotoIdList().size()==0) {
            hideProgress();
            Toast toast = Toast.makeText(getBaseContext(), R.string.nohayresultados, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();

        } else{

            hideProgress();
            Toast toast = Toast.makeText(getBaseContext(), R.string.errorconexion, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();

        }
    }

    @Override
    public void processFinish(DetailResponse photodetails) {

        if (photodetails != null) {

            listphotodetail.add(photodetails.getPhoto());

            if (arrayListphotoid.size() == listphotodetail.size()) {

                cargarFotos(listphotodetail);
            }
        } else {
            hideProgress();
            Toast toast = Toast.makeText(getBaseContext(), R.string.errorconexion, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }

    }

    private void getListPhoidDetail(ArrayList arrayListphotoid) {

        Iterator <String> iterator = arrayListphotoid.iterator();
        while (iterator.hasNext()) {
            String id = iterator.next();
            getPhoidDetail(id, getBaseContext());
        }

    }


    @SuppressLint("WrongConstant")
    private void cargarFotos(ArrayList <Photo> arrayphotodetail) {

        if (arrayphotodetail != null) {
            fotos = arrayphotodetail;


            fotosDel = new ArrayList <>();

            if (fotos.size() >= FOTOS) {

                for (int i = 0; i < FOTOS; i++) {

                    fotosDel.add(fotos.get(i));
                }
            } else {
                for (int i = 0; i < fotos.size(); i++) {
                    fotosDel.add(fotos.get(i));
                }
            }

            PhotoAdapter adapter = new PhotoAdapter(fotosDel);
            rvMain.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, 1, false));
            rvMain.setAdapter(adapter);
            numPag = Math.round((fotos.size()) / FOTOS);
            instaDotView.setNoOfPages((int) numPag);


            rvMain.addOnItemTouchListener(new OnSwipeTouchListener(getBaseContext(),
                    rvMain,
                    new OnSwipeTouchListener.OnTouchActionListener() {
                        @Override
                        public void onLeftSwipe(View view, int position) {
                            page++;
                            if (page > instaDotView.getNoOfPages() - 1)
                                page = instaDotView.getNoOfPages() - 1;
                            instaDotView.onPageChange(page);

                            numpagh = page;

                            ReloadNumPag();
                            ReloadVentajas();

                        }

                        @Override
                        public void onRightSwipe(View view, int position) {

                            page--;
                            if (page < 0) page = 0;
                            instaDotView.onPageChange(page);

                            numpagh = page;

                            ReloadNumPag();

                            ReloadVentajas();
                        }

                    }));

                hideProgress();
        } else {
            hideProgress();
            Toast toast = Toast.makeText(getBaseContext(), R.string.errorconexion, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();

        }

    }

    private void ReloadNumPag() {

        if (numpagh >= numPag) {
            numpagh = 0;
        }
        if (numpagh < 0) {
            numpagh = (int) (numPag - 1);
        }

    }


    @SuppressLint("WrongConstant")
    private void ReloadVentajas() {

        fotosDel = new ArrayList <>();

        if (fotos.size() >= ((numpagh + 1) * FOTOS)) {

            for (int i = (int) (numpagh * FOTOS); i < ((numpagh + 1) * FOTOS); i++) {

                fotosDel.add(fotos.get(i));
            }
        } else {
            for (int i = (int) (numpagh * FOTOS); i < fotos.size(); i++) {
                fotosDel.add(fotos.get(i));
            }
        }

        PhotoAdapter adapter = new PhotoAdapter(fotosDel);
        rvMain.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, 1, false));
        rvMain.setAdapter(adapter);

    }


    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }


    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

}