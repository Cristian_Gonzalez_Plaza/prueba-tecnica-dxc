package es.dxc.flic.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import es.dxc.flic.bean.ApiException;
import es.dxc.flic.bean.Error;


public class RestClient {

    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String APP_ID = "FLICKR_CALL_SERVICES";

    private String baseurl;


    public RestClient(String baseurl) {
        this.baseurl = baseurl;
    }


    public String getUrl() {
        return baseurl;
    }

    public void setUrl(String url) {
        this.baseurl = url;
    }

    public String get(String uri) throws ApiException {
        try {
            HttpURLConnection conn = getConnection(uri, CONTENT_TYPE_JSON);
            conn.connect();
            return processJsonResponse(conn);
        } catch (IOException e) {
            throw processException(e);
        }
    }

    private HttpURLConnection getConnection(String uri, String contentType) {

        Log.i(APP_ID," " + uri);
        Log.d(APP_ID, "Host: " + getUrl());

        String URL = baseurl + uri;
        URL url = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(URL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", contentType);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;

    }

    private ApiException processException(IOException e) {
        if (e instanceof javax.net.ssl.SSLPeerUnverifiedException)
            return new ApiException(ApiException.BAD_SSL_CERTIFICATE, ApiException.BAD_SSL_CERTIFICATE, e);
        else
            return new ApiException(ApiException.ERROR_CALLING_SERVER, ApiException.ERROR_CALLING_SERVER, e);

    }

    private String processJsonResponse(HttpURLConnection conn) throws IOException, ApiException {

        int statusCode = conn.getResponseCode();
        Log.d(APP_ID, "Response code: " + statusCode);
        if (statusCode == 200 || statusCode == 201 || statusCode == 204) {
            //OK
            InputStream is = conn.getInputStream();
            String responseString = read(is);
            Log.d(APP_ID, "Response body: " + responseString);
            return responseString;
        } else if (statusCode == 401 ){
            throw new ApiException(ApiException.BAD_CREDENTIALS, ApiException.BAD_CREDENTIALS);
        }else{
            InputStream is = conn.getErrorStream();
            Error e = Error.fromJSON(read(is));
            String errorMessage = String.valueOf("Error peticion");
            if (e != null && e.getMessage() != null)
                errorMessage = errorMessage + " " + e.getMessage();
            throw new ApiException(errorMessage);
        }
    }


    private static String read(InputStream in) throws IOException {

        InputStreamReader is = new InputStreamReader(in, "UTF8");
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(is);
        String read = br.readLine();

        Log.e("prueba2-read", read.toString());
/*
        while (read.startsWith(UTF8_BOM) || read.startsWith(UTF8_BOM_2)) {
            read = read.substring(1);
        }

 */
        while (read != null) {
            sb.append(read);
            read = br.readLine();

        }
        is.close();

        return sb.toString();
    }



}

