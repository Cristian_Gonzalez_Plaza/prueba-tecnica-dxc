package es.dxc.flic.bean;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Error devuelto por SSLSignature
 */
public class Error {
    private String message;


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public static Error fromJSON(String body) throws ApiException {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            Error aDevolver = new Error();
            aDevolver = mapper.readValue(body,Error.class);
            return aDevolver;
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
