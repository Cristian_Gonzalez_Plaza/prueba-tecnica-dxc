package es.dxc.flic.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Urls implements Serializable {

    @JsonProperty("url")
    private  Url[] Url;


    public Url[] getUrl() {
        return Url;
    }

    public void setUrl(Url[] Url) {
        this.Url = Url;
    }


}














