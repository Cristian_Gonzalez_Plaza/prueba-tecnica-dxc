package es.dxc.flic.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

import java.io.IOException;
import java.util.ArrayList;

import es.dxc.flic.bean.ApiException;
import es.dxc.flic.bean.Photoid;
import es.dxc.flic.bean.PhotoidList;
import es.dxc.flic.bean.SearchResponse;
import es.dxc.flic.util.SessionApi;


public class SearchService {

    String response;

    private static final String API_KEY="682e28f544b26c7b1bb5b5a541c9cd15";

    private static final String LIMITED_PHOTOS="25";

    private static final String SERVICE_SEARCH_FIRST = "method=flickr.photos.search&api_key=";

    private static final String SERVICE_SEARCH_LAST ="&per_page="+LIMITED_PHOTOS+"&format=json&nojsoncallback=1";

    private final ObjectMapper mapper = new ObjectMapper();

    public SearchResponse Idlistphotos (String textobusqueda) throws ApiException {

        String search= "&text="+textobusqueda;


        String uri = String.format(SERVICE_SEARCH_FIRST)+String.format(API_KEY)+String.format(search)+SERVICE_SEARCH_LAST;

            response = SessionApi.getRestClient().get(uri);


        try {
            SearchResponse respuesta = mapper.readValue(response, SearchResponse.class);
            if (respuesta == null ) {
                throw new ApiException("No hay resultados");
            }
            return respuesta;

        } catch (IOException e) {
            e.printStackTrace();
            throw new ApiException(e);
        }

    }

}
