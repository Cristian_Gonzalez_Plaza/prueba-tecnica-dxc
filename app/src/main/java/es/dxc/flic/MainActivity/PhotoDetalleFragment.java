package es.dxc.flic.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.squareup.picasso.Picasso;

import es.dxc.flic.R;
import es.dxc.flic.bean.Photo;

public class PhotoDetalleFragment extends Fragment {

    private View rootView;

    private static final String ARG_FOTO = "arg_foto";

    private TextView TexTitulo, Textautor, Textfecha,Textoparrafo;
    private ImageView Imagen;

    public static PhotoDetalleFragment newInstance(Photo foto) {
        PhotoDetalleFragment fragment = new PhotoDetalleFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_FOTO, foto);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.photo_detalle_fragment, container, false);

        final Photo photo = (Photo) getArguments().getSerializable(ARG_FOTO);

        TexTitulo = rootView.findViewById(R.id.textotitulodetail);
        Textautor = rootView.findViewById(R.id.textoautordetail);
        Textfecha = rootView.findViewById(R.id.textofechadetail);
        Textoparrafo = rootView.findViewById(R.id.textoparrafo);

        Imagen = rootView.findViewById(R.id.ivImagendetail);

        drawFragments(photo);


        Imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                PhotoLastFragment fragment = PhotoLastFragment.newInstance(photo);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.MainActivity, fragment,"fotodetail");
                transaction.addToBackStack("fotodetail");
                transaction.commit();



            }
        });


        return rootView;

    }

    public void drawFragments(Photo photo) {

        if (photo != null &&photo.getTitle().get_Content()!=null) {
            TexTitulo.setText((CharSequence) photo.getTitle().get_Content());
        } else {
            TexTitulo.setText("");
        }

        if (photo != null &&photo.getOwner().getUsername()!=null) {
            Textautor.setText((CharSequence) photo.getOwner().getUsername());
        } else {
            Textautor.setText("");
        }

        if (photo != null &&photo.getDates().getTaken()!=null) {
            Textfecha.setText((CharSequence) photo.getDates().getTaken());
        } else {
            Textfecha.setText("");
        }

        if (photo != null &&photo.getServer()!=null &&photo.getId()!=null&&photo.getSecret()!=null) {
            Picasso.get()
                    .load("https://live.staticflickr.com/" + photo.getServer() + "/" + photo.getId() + "_" + photo.getSecret() + ".jpg")
                    .placeholder(R.drawable.imagen)
                    .error(R.drawable.imagen)
                    .into(Imagen);

        }

        if (photo != null&&photo.getDescription()!=null) {
            Textoparrafo.setText(photo.getDescription().get_Content());
        } else {
            Textoparrafo.setText("");
        }
    }


}

