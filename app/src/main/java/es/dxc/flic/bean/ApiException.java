package es.dxc.flic.bean;


public class ApiException extends Exception {
    public static String BAD_CREDENTIALS = "BAD_CREDENTIALS";
    public static String ERROR_CALLING_SERVER = "ERROR_CALLING_SERVER";
    public static String BAD_SSL_CERTIFICATE = "hostname in certificate didn't match";



    private String code;

    private static final long serialVersionUID = 1L;

    public ApiException() {
        super();
    }


    public ApiException(Throwable cause) {

        super(cause);
    }

    public ApiException(String message) {
        super(message);
    }

    public ApiException(String code, String message) {
        super(message);
        this.code = code;

    }

    public ApiException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public ApiException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }


    public String getCode() {
        return code;
    }

}
