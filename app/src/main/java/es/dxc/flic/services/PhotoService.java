package es.dxc.flic.services;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import es.dxc.flic.bean.ApiException;
import es.dxc.flic.bean.DetailResponse;
import es.dxc.flic.bean.PhotoDetail;
import es.dxc.flic.util.SessionApi;


public class PhotoService {

    String response;


    private static final String API_KEY="682e28f544b26c7b1bb5b5a541c9cd15";

    private static final String SERVICE_SEARCH_FIRST = "method=flickr.photos.getInfo&api_key=";

    private static final String SERVICE_SEARCH_LAST ="&format=json&nojsoncallback=1";




    private final ObjectMapper mapper = new ObjectMapper();

    public DetailResponse getPhotoDetail (String id) throws ApiException {


        String search= "&photo_id="+id;

        String uri = String.format(SERVICE_SEARCH_FIRST)+String.format(API_KEY)+String.format(search)+SERVICE_SEARCH_LAST;



            response = SessionApi.getRestClient().get(uri);


        try {
            DetailResponse respuesta = mapper.readValue(response, DetailResponse.class);
            if (respuesta == null ) {
                throw new ApiException("No hay resultados");
            }
            return respuesta;

        } catch (IOException e) {
            e.printStackTrace();
            throw new ApiException(e);
        }

    }

}
