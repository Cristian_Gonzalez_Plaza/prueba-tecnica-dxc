package es.dxc.flic.bean;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResponse implements Serializable {

    @JsonProperty("photos")
    private PhotoidList photos;


    public PhotoidList getPhotos() {
        return photos;
    }

    public void setPhotos(PhotoidList photos) {
        this.photos = photos;
    }


}














