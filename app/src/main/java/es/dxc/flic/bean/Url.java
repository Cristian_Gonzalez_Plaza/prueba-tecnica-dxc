package es.dxc.flic.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Url implements Serializable {

    @JsonProperty("_content")
    private String _content;


    public String get_Content() {
        return _content;
    }

    public void set_Content(String _content) {
        this._content = _content;
    }


}














