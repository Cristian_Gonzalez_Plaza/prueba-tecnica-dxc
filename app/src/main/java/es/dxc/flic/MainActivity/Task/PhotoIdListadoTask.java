package es.dxc.flic.MainActivity.Task;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.util.ArrayList;

import es.dxc.flic.R;
import es.dxc.flic.bean.ApiException;
import es.dxc.flic.bean.Photoid;
import es.dxc.flic.bean.SearchResponse;
import es.dxc.flic.services.SearchService;



public class PhotoIdListadoTask extends AsyncTask <Void, Void, SearchResponse> {

    private final String Textobusqueda;
    private Context Context;
    SearchService searchservice = new SearchService();
    public AsyncResponse response = null;

    public PhotoIdListadoTask(String textobusqueda,Context context) {
        Textobusqueda = textobusqueda;
        Context = context;
    }

    public interface AsyncResponse {
        void processFinish(SearchResponse photoids);
    }

    @Override
    protected SearchResponse doInBackground(Void... params) {
        // TODO: attempt authentication against a network service.
        SearchResponse photoids = null;
        try {
            photoids = searchservice.Idlistphotos(Textobusqueda);

        } catch (ApiException e) {
            e.printStackTrace();
            return photoids;
        }

        return photoids;
    }

    @Override
    protected void onPostExecute(final SearchResponse success) {

        response.processFinish(success);

    }

    @Override
    protected void onCancelled() {
    }
}
