package es.dxc.flic.MainActivity.Task;


import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.Toast;

import es.dxc.flic.R;
import es.dxc.flic.bean.ApiException;
import es.dxc.flic.bean.DetailResponse;
import es.dxc.flic.services.PhotoService;



public class PhotoIdDetailListadoTask extends AsyncTask <Void, Void, DetailResponse> {

    private String Id;
    private Context Context;
    PhotoService photoService = new PhotoService();
    public AsyncResponse response = null;

    public PhotoIdDetailListadoTask(String id,Context context) {
        Id=id;
        Context = context;
    }

    public interface AsyncResponse {
        void processFinish(DetailResponse photodetails);
    }

    @Override
    protected DetailResponse doInBackground(Void... params) {
        // TODO: attempt authentication against a network service.
        DetailResponse photodetails = null;
        try {
            photodetails = photoService.getPhotoDetail(Id);

        } catch (ApiException e) {
            e.printStackTrace();
            return photodetails;
        }

        return photodetails;
    }

    @Override
    protected void onPostExecute(final DetailResponse success) {

        response.processFinish(success);

    }

    @Override
    protected void onCancelled() {
    }
}
