package es.dxc.flic.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoidList implements Serializable {

    @JsonProperty("photo")
    private ArrayList<Photoid> photo;


    public ArrayList<Photoid> getPhotoIdList() {
        return photo;
    }

    public void setPhotoIdList(ArrayList<Photoid> photo) {
        this.photo = photo;
    }


}














