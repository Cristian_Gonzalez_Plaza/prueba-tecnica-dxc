package es.dxc.flic.MainActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import es.dxc.flic.R;
import es.dxc.flic.bean.Photo;

public class PhotoLastFragment extends Fragment {

    private View rootView;


    private ScaleGestureDetector scaleGestureDetector;
    private float mScaleFactor = 1.0f;

    private static final String ARG_FOTO = "arg_foto";

    private ImageView Imagen;

    public static PhotoLastFragment newInstance(Photo foto) {
        PhotoLastFragment fragment = new PhotoLastFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_FOTO, foto);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mostrar_imagen_full, container, false);

        Photo photo = (Photo) getArguments().getSerializable(ARG_FOTO);

        Imagen = rootView.findViewById(R.id.card_item_background_full);

        scaleGestureDetector = new ScaleGestureDetector(getActivity(), new ScaleListener());

        drawFragments(photo);


        rootView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_MOVE){
                    scaleGestureDetector.onTouchEvent(event);
                    return true;
                }
                return true;
            }
        });


        return rootView;

    }

    public void drawFragments(Photo photo) {


        if (photo != null &&photo.getServer()!=null &&photo.getId()!=null&&photo.getSecret()!=null) {
            Picasso.get()
                    .load("https://live.staticflickr.com/" + photo.getServer() + "/" + photo.getId() + "_" + photo.getSecret() + ".jpg")
                    .placeholder(R.drawable.imagen)
                    .error(R.drawable.imagen)
                    .into(Imagen);
        }

    }



    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));
            Imagen.setScaleX(mScaleFactor);
            Imagen.setScaleY(mScaleFactor);
            return true;
        }
    }

}

