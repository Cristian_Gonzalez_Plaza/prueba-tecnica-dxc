package es.dxc.flic.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Photo implements Serializable {




    @JsonProperty("id")
    private String id;

    @JsonProperty("secret")
    private String secret;

    @JsonProperty("server")
    private String server;

    @JsonProperty("owner")
    private Owner owner;

    @JsonProperty("title")
    private Title title;

    @JsonProperty("description")
    private Descripcion description;

    @JsonProperty("dates")
    private Dates dates;

    @JsonProperty("urls")
    private Urls urls;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }
    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }


    public Descripcion getDescription() {
        return description;
    }

    public void setDescription(Descripcion description) {
        this.description = description;
    }


    public Dates getDates() {
        return dates;
    }

    public void setDates(Dates dates) {
        this.dates = dates;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }
}














