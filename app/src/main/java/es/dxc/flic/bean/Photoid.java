package es.dxc.flic.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Photoid implements Serializable {

    @JsonProperty("id")
    private String id;


    public String getPhotoId() {
        return id;
    }

    public void setPhotoId(String id) {
        this.id = id;
    }


}














