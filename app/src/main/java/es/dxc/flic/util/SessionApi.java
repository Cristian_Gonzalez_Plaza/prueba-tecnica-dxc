package es.dxc.flic.util;



public class SessionApi {


    private static final String urlBase = "https://www.flickr.com/services/rest/?";


    private static RestClient restClient;


    public static RestClient getRestClient() {
        if (restClient == null || !restClient.getUrl().equals(urlBase)){
            restClient = new RestClient(urlBase);
        }
        return restClient;
    }



}
